package com.inonworks.mw.log;

import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.context.annotation.Bean;

public class AroundLogConfiguration {

    @Bean
    public DefaultPointcutAdvisor defaultPointcutAdvisor() {
        AnnotationMatchingPointcut pointcut = AnnotationMatchingPointcut.forMethodAnnotation(AroundLog.class);
        AroundLogInterceptor aroundLogInterceptor = new AroundLogInterceptor();
        DefaultPointcutAdvisor defaultPointcutAdvisor = new DefaultPointcutAdvisor();
        defaultPointcutAdvisor.setPointcut(pointcut);
        defaultPointcutAdvisor.setAdvice(aroundLogInterceptor);
        return defaultPointcutAdvisor;
    }
}
