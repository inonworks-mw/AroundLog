package com.inonworks.mw.log;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(AroundLogConfiguration.class)
public @interface EnableAroundLog {
}
